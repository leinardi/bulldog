Welcome to Sensors Watchdog's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Synopsis
########
sensors_watchdog [-h] [--log-on-stdout] [--notification-sound NOTIFICATION_SOUND] [--refresh-interval REFRESH_INTERVAL] [--shutdown-delay SHUTDOWN_DELAY] [-v] [--version]

Description
###########

What To Monitor
###############

How To Monitor
##############

Logging
#######

Introduction text.

Sample H2
*********

Sample content.

Another H2
**********

And some text.
