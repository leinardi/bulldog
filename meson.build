project('bulldog',
    version: '0.5.0',
    license : 'GPL3',
    meson_version: '>= 0.50.0',
)

python = import('python').find_installation()

sw_prefix = get_option('prefix')
sw_name = meson.project_name()
sw_version = meson.project_version()
sw_bindir = join_paths(sw_prefix, get_option('bindir'))
sw_bin = join_paths(sw_bindir, sw_name)
sw_confdir = join_paths('/', get_option('sysconfdir'), sw_name)
sw_datadir = join_paths(sw_prefix, 'share')
sw_includedir = join_paths(sw_prefix, 'include')
sw_libdir = join_paths(sw_prefix, 'lib', sw_name)
sw_libexecdir = join_paths(sw_prefix, 'libexec', sw_name)
sw_localedir = join_paths(sw_prefix, 'share', 'locale')
sw_mandir = join_paths(sw_prefix, 'share', 'man')
sw_plugindir = join_paths(sw_libdir, sw_version)
sw_python = join_paths(sw_prefix, python.path())
sw_pythondir = join_paths(sw_prefix, python.get_path('purelib'))
sw_rundir = join_paths('/run', sw_name)
sw_sbindir = join_paths(sw_prefix, 'sbin')
sw_vardir = join_paths('/var', 'lib', sw_name)

systemd_dep = dependency('systemd')
systemd_system_unit_dir = systemd_dep.get_pkgconfig_variable('systemdsystemunitdir')

conf = configuration_data()
conf.set('BIN_PATH', sw_bin)
conf.set('CONF_DIR', sw_confdir)
conf.set('PYTHON', sw_python)
conf.set('PYTHON_DIR', sw_pythondir)
conf.set('VERSION', meson.project_version())

install_subdir('bulldog', install_dir: sw_pythondir, exclude_directories: '__pycache__')
subdir('bin')
subdir('etc')
subdir('systemd')

output = '\n'
output += 'System paths:\n'
output += '  bindir: ' + sw_bindir + '\n'
output += '  bin: ' + sw_bin + '\n'
output += '  confdir: ' + sw_confdir + '\n'
output += '  datadir: ' + sw_datadir + '\n'
output += '  libdir: ' + sw_libdir + '\n'
output += '  libexecdir: ' + sw_libexecdir + '\n'
output += '  localedir: ' + sw_localedir + '\n'
output += '  plugindir: ' + sw_plugindir + '\n'
output += '  prefix: ' + sw_prefix + '\n'
output += '  python: ' + sw_python + '\n'
output += '  pythondir: ' + sw_pythondir + '\n'
output += '  rundir: ' + sw_rundir + '\n'
output += '  sbindir: ' + sw_sbindir + '\n'
output += '  systemdunitdir: ' + systemd_system_unit_dir + '\n'
output += '  vardir: ' + sw_vardir + '\n'

message(output)

