# Bulldog
Bulldog is a CLI tool that automatically shutdown the system when a sensor goes outside the desired range.

## 💡 Features

* monitor multiple sensors, each with its own range
* automatically shutdown the system when one of the sensors goes outside the min/max range
* delay the shutdown for the desired amount of seconds
* notify all the connected GUI users when a sensor fails
* play a sound for all the connected GUI users while the sensor is still failing
* cancel the delayed shutdown if the sensor goes back inside the min/max range
* integration with systemd (start on boot and log on journal)

## 📦 How to get Bulldog
### Install from source code
#### Build time dependencies
| Distro                | pkg-config         | Python 3.6+ | pip         | meson | ninja-build |
| --------------------- | ------------------ | ----------- | ----------- | ----- | ----------- |
| Arch Linux            | pkg-config         | python      | python3-pip | meson | ninja       |
| Fedora                | pkgconf-pkg-config | python3     | python3-pip | meson | ninja-build |
| Ubuntu                | pkg-config         | python3     | python3-pip | meson | ninja-build |

plus all the Python dependencies listed in [requirements.txt](requirements.txt)

#### Clone project and install
If you have not installed Bulldog yet:
```bash
git clone --recurse-submodules -j4 https://gitlab.com/leinardi/bulldog.git
cd bulldog
git checkout release
sudo -H pip3 install -r requirements.txt
meson . build --prefix /usr
ninja -v -C build
ninja -v -C build install
```

#### Update old installation
If you installed Bulldog from source code previously and you want to update it:
```bash
cd bulldog
git fetch
git checkout release
git reset --hard origin/release
git submodule init
git submodule update
sudo -H pip3 install -r requirements.txt
meson . build --prefix /usr
ninja -v -C build
ninja -v -C build install
```

#### Run
Once installed, to start it you can simply execute on a terminal:
```bash
bulldog
```

<!-- ## ❓ FAQ -->

## 💚 How to help the project
<!-- ### Discord server
If you want to help testing or developing it would be easier to get in touch using the discord server of the project: https://discord.gg/wjBH8w3  
Just write a message on the general channel saying how you want to help (test, dev, etc) and quoting @leinardi. If you don't use discor but still want to help just open a new issue here.
-->

### Can I support this project some other way?

Something simple that everyone can do is to star it on both [GitLab](https://gitlab.com/leinardi/bulldog) and [GitHub](https://github.com/leinardi/bulldog).
Feedback is always welcome: if you found a bug or would like to suggest a feature,
feel free to open an issue on the [issue tracker](https://gitlab.com/leinardi/bulldog/issues).

<!-- ## 📰 Media coverage --> 

## 📝 License
```
This file is part of Bulldog.

Copyright (c) 2020 Roberto Leinardi

Bulldog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bulldog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bulldog.  If not, see <http://www.gnu.org/licenses/>.
```
