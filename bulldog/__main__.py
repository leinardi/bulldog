# This file is part of Bulldog.
#
# Copyright (c) 2020 Roberto Leinardi
#
# Bulldog is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Bulldog is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bulldog.  If not, see <http://www.gnu.org/licenses/>.
import argparse
import logging
import os
import re
import signal
import subprocess
import sys
import threading
from dataclasses import dataclass
from enum import Enum, IntEnum, unique
from os.path import isfile
from pwd import getpwnam
from threading import Event
from typing import Optional, List, Dict, Any, Union

from systemd.journal import JournalHandler

_LOG = logging.getLogger('Bulldog')
_STDOUT_HANDLER = logging.StreamHandler(sys.stdout)
_FORMATTER = logging.Formatter('%(asctime)s - %(name)s - %(levelname)8s - %(message)s')
_STDOUT_HANDLER.setFormatter(_FORMATTER)
_JOURNAL_HANDLER = JournalHandler()
_LOG.addHandler(_JOURNAL_HANDLER)
_LOG.setLevel(logging.INFO)

_CONFIG_FILE = "bulldog.conf"
_SENSOR_TAB_FILE = "sensor_tab.conf"

_NOTIFY_SEND_COMMAND = "sudo -u {} " \
                       "DISPLAY={} " \
                       "DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/{}/bus " \
                       "PATH={} " \
                       "notify-send \"{}\" \"{}\" --urgency={}"

_PAPLAY_COMMAND = "sudo -u {} " \
                  "XDG_RUNTIME_DIR=/run/user/{} " \
                  "paplay {}"

_EXIT = Event()


def exit_gracefully(signum: int, _: Any) -> None:
    signal.signal(signum, signal.SIG_IGN)  # ignore additional signals
    _EXIT.set()


signal.signal(signal.SIGINT, exit_gracefully)
signal.signal(signal.SIGTERM, exit_gracefully)


@dataclass
class Config:
    log_on_stdout: bool = False
    notification_sound: str = '/usr/share/sounds/freedesktop/stereo/dialog-error.oga'
    refresh_interval: int = 5
    shutdown_delay: int = 30
    verbose: bool = False


@dataclass
class MonitoredItem:
    path: str
    min_value: Optional[int]
    max_value: Optional[int]
    last_value: Optional[int] = None
    is_failing: bool = False


@dataclass
class User:
    username: str
    uid: int
    display: List[str]


@unique
class NotificationLevel(Enum):
    LOW = 'low'
    NORMAL = 'normal'
    CRITICAL = 'critical'


@unique
class ExitStatus(IntEnum):
    SUCCESS = 0
    ERROR_GENERIC = 1
    ERROR_PARSING_CONFIG = 23
    ERROR_SENSOR_TAB = 29


class Bulldog:

    def __init__(
            self,
            version: str,
            conf_dir: str,
    ) -> None:
        self._version = version
        self._conf_dir = conf_dir
        self._config: Config = Config()
        self._sensors_failing: int = 0
        self._timer = threading.Timer(self._config.shutdown_delay, self._shutdown_now)
        self._parse_config()
        self._parse_args()
        self._apply_config()
        self._monitored_items: List[MonitoredItem] = self._parse_sensor_tab()
        self._start_monitoring()

    def _apply_config(self) -> None:
        if self._config.log_on_stdout:
            _LOG.removeHandler(_JOURNAL_HANDLER)
            _LOG.addHandler(_STDOUT_HANDLER)
        if self._config.verbose:
            _LOG.setLevel(logging.DEBUG)

    def _parse_config(self) -> None:
        try:
            with open(os.path.join(self._conf_dir, _CONFIG_FILE), 'r') as file:
                config_dict: Dict[str, Union[int, str]] = {}
                for line in file:
                    if line.isspace() or line.startswith('#'):
                        continue
                    key, value = line.rstrip().split()
                    config_dict[key] = int(value) if value.isnumeric() else str(value)

                if 'LogOnStdout' in config_dict:
                    self._config.log_on_stdout = self._parse_bool(str(config_dict['LogOnStdout']))
                if 'NotificationSound' in config_dict:
                    self._config.notification_sound = str(config_dict['NotificationSound'])
                if 'RefreshInterval' in config_dict:
                    self._config.refresh_interval = int(config_dict['RefreshInterval'])
                if 'ShutdownDelay' in config_dict:
                    self._config.shutdown_delay = int(config_dict['ShutdownDelay'])
                if 'Verbose' in config_dict:
                    self._config.verbose = self._parse_bool(str(config_dict['Verbose']))

                _LOG.info("Configuration loaded:\n%s", self._config)
        except FileNotFoundError:
            _LOG.exception("Unable to read the configuration file.")
            sys.exit(ExitStatus.ERROR_PARSING_CONFIG)
        except ValueError:
            _LOG.exception("Unable to parse the config file.")
            sys.exit(ExitStatus.ERROR_PARSING_CONFIG)

    def _parse_sensor_tab(self) -> List[MonitoredItem]:
        try:
            with open(os.path.join(self._conf_dir, _SENSOR_TAB_FILE), 'r') as file:
                monitored_items: List[MonitoredItem] = []
                for line in file:
                    if line.isspace() or line.startswith('#'):
                        continue
                    path, min_value, max_value = line.rstrip().split()
                    if not isfile(path) or not os.access(path, os.R_OK):
                        _LOG.error("sensor_tab.conf error: File %s doesn't exist or isn't readable. "
                                   "This entry will be ignored.", path)
                    else:
                        try:
                            monitored_item = MonitoredItem(
                                path=path,
                                min_value=self._parse_sensor_tab_value(min_value),
                                max_value=self._parse_sensor_tab_value(max_value)
                            )
                            monitored_items.append(monitored_item)
                        except ValueError:
                            _LOG.error("sensor_tab.conf error: Unable to parse min/max values (%s/%s) of %s. "
                                       "Accepted values are integer numbers or the word \"None\". "
                                       "This entry will be ignored.",
                                       min_value,
                                       max_value,
                                       path)
                _LOG.info("Sensor tab parsed:\n%s", monitored_items)
                return monitored_items
        except FileNotFoundError:
            _LOG.exception("Unable to read the sensor tab file.")
            sys.exit(ExitStatus.ERROR_SENSOR_TAB)

    @staticmethod
    def _parse_sensor_tab_value(value: str) -> Optional[int]:
        if value.lower() == 'None'.lower():
            return None
        return int(value)

    def _parse_args(self) -> None:
        parser = argparse.ArgumentParser(description='Short description')
        parser.add_argument('--log-on-stdout', help="output the logs on stdout instead of the journal",
                            action="store_true")
        parser.add_argument('--notification-sound', type=argparse.FileType('r', encoding='UTF-8'),
                            help="path to the file that should be played when a notification is sent")
        parser.add_argument('--refresh-interval', help="refresh interval in seconds", type=int)
        parser.add_argument('--shutdown-delay', help="shutdown delay in seconds", type=int)
        parser.add_argument('-v', '--verbose', help="increase output verbosity", action="store_true")
        parser.add_argument('--version', action='version', version=self._version)
        args = parser.parse_args()
        if args.log_on_stdout:
            self._config.log_on_stdout = True
        if args.notification_sound is not None:
            self._config.notification_sound = args.notification_sound.name
        if args.refresh_interval is not None:
            self._config.refresh_interval = args.refresh_interval
        if args.shutdown_delay is not None:
            self._config.shutdown_delay = args.shutdown_delay
        if args.verbose:
            self._config.verbose = True

    def _start_monitoring(self) -> None:
        _LOG.info("Monitoring started")
        while not _EXIT.is_set():
            _LOG.debug('Checking sensors:')
            for monitored_item in self._monitored_items:
                value = self._read_monitored_item_value(monitored_item)
                self._check_value(monitored_item, value)
                _LOG.debug(monitored_item)
            _EXIT.wait(self._config.refresh_interval)
        _LOG.warning("Monitoring interrupted. Exiting...")
        sys.exit(ExitStatus.SUCCESS)

    @staticmethod
    def _read_monitored_item_value(monitored_item: MonitoredItem) -> int:
        with open(monitored_item.path, 'r') as file:
            return int(file.read())

    def _check_value(self, monitored_item: MonitoredItem, value: int) -> None:
        is_failing_now = False

        if monitored_item.min_value is not None and value <= monitored_item.min_value:
            _LOG.debug('Sensor below minimum allowed value')
            is_failing_now = True
            if not monitored_item.is_failing:
                monitored_item.is_failing = True
                self._start_shutdown(monitored_item, value)

        if monitored_item.max_value is not None and value >= monitored_item.max_value:
            _LOG.debug('Sensor above minimum allowed value')
            is_failing_now = True
            if not monitored_item.is_failing:
                monitored_item.is_failing = True
                self._start_shutdown(monitored_item, value)

        if monitored_item.is_failing and not is_failing_now:
            monitored_item.is_failing = False
            self._cancel_shutdown(monitored_item, value)
        elif is_failing_now:
            self._play_sound_all(self._get_user_list())
        monitored_item.last_value = value

    def _start_shutdown(self, monitored_item: MonitoredItem, value: int) -> None:
        if self._sensors_failing == 0:
            title = "🚨 Bulldog: sensor failure!"
            message = "The system will be forcibly shutdown in {} seconds!\n\n" \
                      "Sensor {} failure\n(current: {}, min: {}, max: {})".format(
                self._config.shutdown_delay,
                monitored_item.path,
                value,
                str(monitored_item.min_value),
                str(monitored_item.max_value)
            )
            _LOG.error(re.sub('\n+', ' ', message))
            self._notify_send_all(self._get_user_list(), title, message, NotificationLevel.CRITICAL)
            self._timer.start()
            self._sensors_failing += 1
        else:
            _LOG.debug('This is not the first sensor failing, a shutdown should be already scheduled.')

    @staticmethod
    def _shutdown_now() -> None:
        _LOG.error("Shutting down now!")
        os.system("systemctl poweroff")

    def _cancel_shutdown(self, monitored_item: MonitoredItem, value: int) -> None:
        if self._sensors_failing > 0:
            _LOG.debug('Decreasing sensors failing counter')
            self._sensors_failing -= 1
            if self._sensors_failing == 0:
                title = "ℹ️ Bulldog: sensor values back to normal"
                message = "System shutdown has been cancelled.\n\n" \
                          "Sensor {} value is back inside the valid range\n(current: {}, min: {}, max: {})".format(
                    monitored_item.path,
                    value,
                    str(monitored_item.min_value),
                    str(monitored_item.max_value)
                )
                _LOG.warning(re.sub('\n+', ' ', message))
                self._notify_send_all(self._get_user_list(), title, message, NotificationLevel.CRITICAL)
                self._timer.cancel()
                self._timer = threading.Timer(self._config.shutdown_delay, self._shutdown_now)
        else:
            _LOG.error("Trying to cancel a shutdown with no sensors failing. This should never happen."
                       "Please report this bug to the software maintainer.")

    @staticmethod
    def _get_user_list() -> List[User]:
        who_output = subprocess.run(['who', '-s'], stdout=subprocess.PIPE, check=True).stdout.decode(
            'utf-8')
        _LOG.debug("who output:\n%s", who_output)
        regex = re.compile(r'\(*:\d+\)')
        temp_dict: Dict[str, User] = {}
        for line in filter(regex.search, who_output.splitlines()):
            words = line.split()
            username = words[0]
            uid = getpwnam(username).pw_uid
            display = words[-1][1:-1]
            if username in temp_dict:
                _LOG.debug("Adding display %s to user %s", display, username)
                temp_dict[username].display.append(display)
            else:
                _LOG.debug("Creating user %s (uid %s) with display %s", username, uid, display)
                temp_dict[username] = User(
                    username=username,
                    uid=uid,
                    display=[display]
                )

        return list(temp_dict.values())

    @staticmethod
    def _notify_send_all(user_list: List[User],
                         title: str,
                         message: str,
                         level: NotificationLevel) -> None:
        _LOG.debug('Showing notifications')
        for user in user_list:
            for display in user.display:
                cmd = _NOTIFY_SEND_COMMAND.format(
                    user.username,
                    display,
                    user.uid,
                    os.environ['PATH'],
                    title,
                    message,
                    level.value
                )
                _LOG.debug("Notifying user %s on display %s", user.username, display)
                _LOG.debug(cmd)
                os.system(cmd)

    def _play_sound_all(self, user_list: List[User]) -> None:
        _LOG.debug('Playing sounds')
        for user in user_list:
            cmd = _PAPLAY_COMMAND.format(
                user.username,
                user.uid,
                self._config.notification_sound
            )
            _LOG.debug("Playing sound for user %s", user.username)
            _LOG.debug(cmd)
            os.system(cmd)

    @staticmethod
    def _parse_bool(sting: str) -> bool:
        return sting.lower() in ['true', '1', 'y', 'yes', 'on']
